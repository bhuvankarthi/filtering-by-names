import got from "./data.js";

let nameList = document.getElementById("name-list-id");
let ulist = document.getElementById("ul-list");
let userInput = document.getElementById("user-input-id");

let target = "";

function creatingElement(names) {
  // console.log(names);
  for (let i = 0; i < names.people.length; i++) {
    // if (names[i].name.includes(userInput.value.trim())) {
    let data = document.createElement("div");
    let image = document.createElement("img");
    image.src = names.people[i].image;
    image.classList.add("image-class");
    data.appendChild(image);
    ulist.appendChild(data);

    let nameHeading = document.createElement("h3");
    nameHeading.textContent = names.people[i].name;
    data.appendChild(nameHeading);

    let about = document.createElement("p");
    about.textContent = names.people[i].description;
    data.appendChild(about);

    let anchor = document.createElement("a");
    anchor.textContent = "Know more!";
    anchor.setAttribute("href", names.wikiLink);
    anchor.classList.add("anchor-tags");
    data.appendChild(anchor);
    // }
  }
}

got.houses.map((names) => {
  let name = document.createElement("div");
  name.append(names.name);
  nameList.appendChild(name);
  nameList.classList.add("names-of-actors");
  if (!target) {
    creatingElement(names);
  }

  name.addEventListener("click", (event) => {
    target = names.name;
    while (ulist.lastChild) {
      ulist.removeChild(ulist.lastChild);
    }
    if (names.name === target) {
      console.log(names);
      creatingElement(names);
    }
  });

  userInput.addEventListener("keyup", (event) => {
    if (event.target.value) {
      while (ulist.lastChild) {
        ulist.removeChild(ulist.lastChild);
      }
      got.houses.filter((item) => {
        item.people.filter((people, index) => {
          let nameOfPerson = people.name.toLowerCase();
          let inputValue = event.target.value.toLowerCase();
          if (nameOfPerson.includes(inputValue)) {
            // console.log(item.people[index]);
            let object = {};
            let array = [];
            array.push(item.people[index]);
            object["people"] = array;

            // console.log(object.people);
            creatingElement(object);
          }
        });
      });
    }
  });
});
